const { expect } = require("chai");
let chai = require("chai");
let chaiHttp = require("chai-http");
let server = 'http://localhost:3000';

//Assertion Style
chai.expect();

chai.use(chaiHttp);

describe('Users API', () => {

  context('GET /api/users', () => {
    it('It should GET all the users', (done) => {
      chai.request(server)
        .get('/api/users')
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body).to.have.length(1);
          done();
        });
    });

    it('It should NOT GET all the users', (done) => {
      chai.request(server)
        .get("/api/user")
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });
  });

  context('GET /api/users/:id', () => {
    it('It should GET a user by ID', (done) => {
      const id = 1;
      chai.request(server)
        .get('/api/users/' + id)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body).to.have.property('id');
          expect(res.body).to.have.property('email');
          expect(res.body).to.have.property('first_name');
          expect(res.body).to.have.property('last_name');
          expect(res.body).to.have.property('avatar');
          expect(res.body).to.have.property('id').eq(1);
          done();
        });
    });

    it('It should NOT GET a user by ID', (done) => {
      const id = 123;
      chai.request(server)
        .get('/api/users/' + id)
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });
  });

  context('POST /api/users', () => {
    it('It should POST a new user', (done) => {
      const user = {
        email: "marianne.lim85@gmail.com",
        first_name: "Marianne",
        last_name: "Lim",
        avatar: ""
      };

      chai.request(server)
        .post('/api/users')
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(201);
          expect(res).to.be.json;
          expect(res.body).to.have.property('id').eq(2);
          expect(res.body).to.have.property('first_name').eq("Marianne");
          expect(res.body).to.have.property('avatar').eq('');
          done();
        });
    });

    it('It should NOT POST a new user without the other require field', (done) => {
      const id = 2;
      const user = {
        first_name: "Marianne",
      };
      chai.request(server)
        .post('/api/users' + id)
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });
  });

  context('PUT /api/users/:id', () => {
    it('It should PUT an existing user', (done) => {
      const id = 2;
      const user = {
        email: "marianne.lim85@gmail.com",
        first_name: "Marianne",
        last_name: "Lim",
        avatar: "test-pic.png"
      };

      chai.request(server)
        .put('/api/users/' + id)
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body).to.have.property('id').eq(2);
          expect(res.body).to.have.property('first_name').eq('Marianne');
          expect(res.body).to.have.property('avatar').eq('test-pic.png');
          done();
        });
    });

    it('It should NOT PUT a an existing user without the other require field', (done) => {
      const id = 2;
      const user = {
        avatar: "test-pic.png",
      };
      chai.request(server)
        .put('/api/users/' + id)
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });
  });

  context('PATCH /api/users/:id', () => {
    it('It should PATCH an existing user', (done) => {
      const id = 2;
      const user = {
        avatar: "This is a Patch"
      };

      chai.request(server)
        .patch('/api/users/' + id)
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res.body).to.have.property('id').eq(2);
          expect(res.body).to.have.property('avatar').eq('This is a Patch');
          done();
        });
    });

    it('It should NOT PATCH a an existing user without the other require field', (done) => {
      const id = 123;
      const user = {
        avatar: "test-pic.png",
      };
      chai.request(server)
        .patch('/api/users/' + id)
        .send(user)
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });
  });

  context('DELETE /api/users/:id', () => {
    it('It should DELETE a user', (done) => {
      const id = 2;
      chai.request(server)
        .delete('/api/users/' + id)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('It should NOT DELETE any existing user', (done) => {
      const id = 123;
      chai.request(server)
        .delete('/api/users/' + id)
        .end((err, res) => {
          expect(res).to.have.status(404);
          done();
        });
    });
  });

});