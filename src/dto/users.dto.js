const Joi = require('joi');

const schema = Joi.object().keys({
  email: Joi.string().required(),
  first_name: Joi.string().required(),
  last_name: Joi.string().required(),
  avatar: Joi.string().allow('')
});

const patchSchema = Joi.object().keys({
  email: Joi.string().allow(''),
  first_name: Joi.string().allow(''),
  last_name: Joi.string().allow(''),
  avatar: Joi.string().allow('')
});

module.exports = {
  schema,
  patchSchema
}