const express = require('express');
const dto = require('./dto/users.dto');
const { findAll, findOneById } = require('./users.controller');

const app = express();
app.use(express.json());

const users = findAll();

app.get('/api/users', (req, res) => {
  const users = findAll();
  res.send(users);
});

app.get('/api/users/:id', (req, res) => {
  const user = findOneById(req.params.id);
  if (!user) {
    res.status(404).send('User not found');
  } else {
    res.send(user);
  }
});

app.post('/api/users', (req, res) => {
  const validation = dto.schema.validate(req.body);

  if (validation.error) res.status(404).send(validation.error.details[0]);

  const user = {
    id: users.length + 1,
    email: req.body.email,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    avatar: req.body.avatar
  };
  users.push(user);
  res.status(201).send(user);
});

app.put('/api/users/:id', (req, res) => {
  const user = findOneById(req.params.id);
  const validation = dto.schema.validate(req.body);

  if (!user) res.status(404).send('User not found');
  if (validation.error) res.status(404).send(validation.error.details[0]);

  user.email = req.body.email;
  user.first_name = req.body.first_name;
  user.last_name = req.body.last_name;
  user.avatar = req.body.avatar;

  res.send(user)

});

app.patch('/api/users/:id', (req, res) => {
  const user = findOneById(req.params.id);
  const validation = dto.patchSchema.validate(req.body);

  if (!user) res.status(404).send('User not found');
  if (validation.error) res.status(404).send(validation.error.details[0]);

  if (req.body.email) {
    user.email = req.body.email;
  }

  if (req.body.first_name) {
    user.first_name = req.body.first_name;
  }

  if (req.body.last_name) {
    user.last_name = req.body.last_name;
  }

  if (req.body.avatar) {
    user.avatar = req.body.avatar;
  }

  res.send(user)

});

app.delete('/api/users/:id', (req, res) => {
  const user = findOneById(req.params.id);
  if (!user) {
    res.status(404).send(`User ${req.params.id} not found`);
  } else {
    const index = users.indexOf(user);
    users.splice(index, 1);
    res.send(`User ${req.params.id} has been deleted`);
  }
});


module.exports = app;
